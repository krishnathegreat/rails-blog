class Article < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
 
 validates :title, presence: true,
                    length: { minimum: 5 }
 validates :text, presence: true,length:{minimum:4}
has_many :comments, dependent: :destroy
end
