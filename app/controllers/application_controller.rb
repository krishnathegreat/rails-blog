class ApplicationController < ActionController::Base
before_action :authenticate_user!
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
def mailer_set_url_options
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end
  protect_from_forgery with: :exception
end
